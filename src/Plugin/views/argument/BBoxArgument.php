<?php

namespace Drupal\views_geojson\Plugin\views\argument;

use Drupal\views\Plugin\views\join\JoinPluginBase;
use Drupal\views\Plugin\views\argument\StringArgument;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler for Bounding Boxes.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("views_geojson_bbox_argument")
 */
class BBoxArgument extends StringArgument implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   *
   * @todo Is this correct?
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['round_coordinates'] = ['default' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    unset($form['case']);
    unset($form['path_case']);
    $form['description']['#markup'] .= $this->t('<br><strong>The format should be: <em>"left,bottom,right,top"</em></strong>.');
    $form['round_coordinates'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Round coordinates'),
      '#default_value' => $this->options['title'],
      '#description' => $this->t('Round coordinates to two decimal places. This can help in caching bounding box queries. For instance, "-0.542,51.344,-0.367,51.368" and "-0.541,51.343,-0.368,51.369" would use the same SQL query.'),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Add WHERE clause to query to implement bounding box filter.
   */
  public function query($group_by = FALSE) {
    if ($bbox = $this->getParsedBoundingBox()) {
      $geojson_options = $this->view->getStyle()->options;

      // Identify geodata source tables & fields based on data source type.
      if (!empty($geojson_options['data_source']['value'])) {
        switch ($geojson_options['data_source']['value']) {
          case 'latlon':
            $geo_tables[] = $this->view->field[$geojson_options['data_source']['latitude']]->table;
            $geo_tables[] = $this->view->field[$geojson_options['data_source']['longitude']]->table;
            $geo_table_entity_id_field = 'entity_id';
            $field_lat = $this->view->field[$geojson_options['data_source']['latitude']]->realField;
            $field_lng = $this->view->field[$geojson_options['data_source']['longitude']]->realField;
            break;

          case 'geofield':
            // @todo Geofields can be more than just points
            $geo_tables[] = $this->view->field[$geojson_options['data_source']['geofield']]->table;
            $geo_table_entity_id_field = 'entity_id';
            $field_lat = $geojson_options['data_source']['geofield'] . '_lat';
            $field_lng = $geojson_options['data_source']['geofield'] . '_lon';
            break;

          case 'geolocation':
            // @todo Geolocations can be more than just points
            $geo_tables[] = $this->view->field[$geojson_options['data_source']['geolocation']]->table;
            $geo_table_entity_id_field = 'entity_id';
            $field_lat = $geojson_options['data_source']['geolocation'] . '_lat';
            $field_lng = $geojson_options['data_source']['geolocation'] . '_lng';
            break;

          case 'wkt':
            // @todo Implement WKT
          default:
            return;
        }
      }
      else {
        return;
      }

      // Get the appropriate tables into the query.
      foreach ($geo_tables as $geo_table) {
        $this->query->ensureTable($geo_table, NULL, new JoinPluginBase([
          'table' => $geo_table,
          'field' => $geo_table_entity_id_field,
          'left_table' => 'node_field_data',
          'left_field' => 'nid',
        ],
          NULL, NULL));
      }

      // Add the WHERE clauses.
      $this->query->addWhere('bbox', $field_lat, $bbox['bottom'], '>=');
      $this->query->addWhere('bbox', $field_lat, $bbox['top'], '<=');
      $this->query->addWhere('bbox', $field_lng, $bbox['left'], '>=');
      $this->query->addWhere('bbox', $field_lng, $bbox['right'], '<=');
    }
  }

  /**
   * Parses the bounding box argument.
   *
   * Parses the bounding box argument. Returns an array keyed 'top', 'left',
   * 'bottom', 'right' or FALSE if the argument was not parsed succesfully.
   *
   * @return array|bool
   *   The calculated values.
   */
  public function getParsedBoundingBox() {
    static $values;

    if (!isset($values)) {
      $exploded_values = explode(',', $this->getValue());
      if (count($exploded_values) == 4) {
        $values['left'] = (float) $exploded_values[0];
        $values['bottom'] = (float) $exploded_values[1];
        $values['right'] = (float) $exploded_values[2];
        $values['top'] = (float) $exploded_values[3];

        if ($this->options['round_coordinates']) {
          $values['left'] -= 0.005;
          $values['bottom'] -= 0.005;
          $values['right'] += 0.005;
          $values['top'] += 0.005;
          foreach ($values as $k => $v) {
            $values[$k] = round($values[$k], 2);
          }
        }
      }
      else {
        $values = FALSE;
      }
    }
    return $values;
  }

}
